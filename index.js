const express = require('express');
const app = express();

app.get('/results/:n1/:n2', (req, res) => {
    const {n1, n2} = req.params;
    res.json(Number(n1) + Number(n2));
});

app.post('/results/:n1/:n2', (req, res) => {
    const {n1, n2} = req.params;
    res.json(Number(n1) * Number(n2));
});

app.put('/results/:n1/:n2', (req, res) => {
    const {n1, n2} = req.params;
    res.json(Number(n1) / Number(n2));
});

app.patch('/results/:n1/:n2', (req, res) => {
    const {n1, n2} = req.params;
    res.json(Number(n1) ** Number(n2));
});

app.delete('/results/:n1/:n2', (req, res) => {
  const { n1, n2 } = req.params;
  res.json(Number(n1) - Number(n2));
});

app.listen(3000);
