# Tarea | Tablas RESTful

Creaciòn de API REST

## Peticiòn

Crear API REST utilizando la siguiente tabla RESTFull:

* GET /results/:n1/:n2 -> Sumar n1 + n2
* POST /results/ -> Multiplicar n1 * n2
* PUT /results/ -> Dividir n1 / n2
* PATCH /results/ -> Potencia n1 ^ n2
* DELETE /results/:n1/:n2 -> restar n1 - n2

## Prerrequisitos

* node
* postman

## Instrucciones

* Clonar el repositorio
* Ejecutar dentro del repositorio clonado:

```
node index.js
```

Abrir Postman y mediante http poner la direcciòn:

```
http://localhost:3000/results/{nùmero 1}/{nùmero 2}
```

Ejemplo:

```
http://localhost:3000/results/3/2
```

## Resultados

### GET

**Devuelve la suma**

Ejemplo:

```
http://localhost:3000/results/3/2
```

Resultado

```
5
```

### POST

**Devuelve la multiplicaciòn**

Ejemplo:

```
http://localhost:3000/results/3/2
```

Resultado

```
6
```

### PUT

**Devuelve la diviciòn del primer nùmero entre el segundo**

Ejemplo:

```
http://localhost:3000/results/3/2
```

Resultado

```
1.5
```

### PATCH

**Devuelve el primer nùmero a la potencia del segundo**

Ejemplo:

```
http://localhost:3000/results/3/2
```

Resultado

```
9
```

### DELETE

**Devuelve la resta del primer nùmero menos el segundo**

Ejemplo:

```
http://localhost:3000/results/3/2
```

Resultado

```
1
```

## Autor

* **Daniel Josue Lozano Porras** 348603

